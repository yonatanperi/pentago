import Tkinter as tk
from PIL import Image, ImageTk


class GraphicBoard(object):

    def __init__(self, board_size=6):
        self.directions = ("right", "left")
        self.board_size = board_size
        self.dots = []

        self.rotation_buttons = {}
        for quarter in range(4):
            self.rotation_buttons[quarter] = {self.directions[0]: None,
                                              self.directions[1]: None}
        """
        {quarter_num: {'left': left_btn, 'right': right_btn}}
        """

    def import_all_imgs(self):

        def import_img(img_location, img_size):
            pil_img = Image.open(img_location)
            pil_img.thumbnail(img_size, Image.ANTIALIAS)
            img = ImageTk.PhotoImage(pil_img)
            return img

        # import the dots imgs from the circles folder
        dots_imgs = []
        size = (50, 50)
        for dot_color in range(3):
            dot_img = import_img(
                img_location="imgs/circles/{0}.png".format(dot_color),
                img_size=size)
            dots_imgs.append(dot_img)

        # import rotation imgs
        rotation_imgs = {}
        """
        {"right": right_rotation_img}
        {"left": left_rotation_img}
        """
        for direction in self.directions:
            rotation_img = import_img(
                img_location="imgs/rotation/{0}.png".format(direction),
                img_size=size)
            rotation_imgs[direction] = rotation_img

        # import lines imgs
        lines_imgs = {}
        """
        "horizontal_line": horizontal_line_img
        "vertical_line": vertical_line_img
        "center_line": center_line_img
        """
        line_kinds = ("horizontal_line",
                      "vertical_line",
                      "center_line")
        for line_kind in line_kinds:
            line_img = import_img(
                img_location="imgs/lines/{0}.png".format(line_kind),
                img_size=size)
            lines_imgs[line_kind] = line_img

        return {
            "dots_imgs": dots_imgs,
            "rotation_imgs": rotation_imgs,
            "lines_imgs": lines_imgs
        }

    def flip_directions(self, input_direction):

        for direction in range(2):
            if input_direction == self.directions[direction]:
                return self.directions[-1 * direction + 1]  # flips the directions

        return None  # if <input_direction> isn't in <self.directions>, the function returns None

    def open_window(self):
        root = tk.Tk()
        root.title("Pentago")
        root.minsize(width=600, height=600)
        root.grid_propagate(False)

        horizontal_rotation_buttons = []
        vertical_rotation_buttons = []

        imgs = self.import_all_imgs()

        dots_imgs = imgs["dots_imgs"]
        rotation_imgs = imgs["rotation_imgs"]
        lines_imgs = imgs["lines_imgs"]

        def create_button(image, row, column, command, padx=5, pady=5, **kwargs):

            tk_button = tk.Button(root, image=image)
            tk_button.config(command=lambda widget=tk_button: command(widget))

            if kwargs is not None:
                for key, value in kwargs.iteritems():
                    tk_button[str(key)] = value

            tk_button.grid(row=row,
                           column=column,
                           padx=padx,
                           pady=pady)
            return tk_button

        rotation_locations = {
            "right": [(0, 0),
                      (self.board_size, 0)],

            "left": [(0, self.board_size),
                     (self.board_size, self.board_size)]
        }

        # create the vertical rotation buttons
        for direction in rotation_locations.keys():
            for i in range(2):
                rotation_button = create_button(image=rotation_imgs[self.flip_directions(direction)],
                                                row=(self.board_size + 2) * i,
                                                # |^ (0, 0)
                                                #    (1, s + 2)
                                                column=rotation_locations[direction][i][1] + 1,
                                                command=self.rotation_click,
                                                state="disabled")

                # appends the created button to the vertical rotation buttons list:
                vertical_rotation_buttons.append(rotation_button)

        # create the other buttons and the spaces between them
        for line in range(self.board_size + 1):
            if line == 3:  # create the horizontal lines and center line between the dots
                for line_column in range(self.board_size + 1):
                    if line_column == 3:
                        tk.Label(root, image=lines_imgs["center_line"]).grid(row=line + 1, column=line_column + 1)
                        line_column += 1

                    tk.Label(root, image=lines_imgs["horizontal_line"]).grid(row=line + 1, column=line_column + 1)
                self.dots.append([])

                line += 1

            self.dots.append([])  # adds a new line
            for column in range(self.board_size + 1):

                if column == 3:  # create the vertical lines and center line between the dots
                    if line != 3:
                        tk.Label(root, image=lines_imgs["vertical_line"]).grid(row=line + 1, column=column + 1)
                        continue

                # create the horizontal rotation buttons
                for direction in rotation_locations.keys():
                    if (line, column) in rotation_locations[direction]:
                        rotation_button = create_button(image=rotation_imgs[direction],
                                                        row=(self.board_size * line) / self.board_size + 1,
                                                        # |^ (0, 1)
                                                        #    (s, s + 1)
                                                        column=column * (self.board_size + 3) / self.board_size,
                                                        # |^ (0, 0)
                                                        #    (s, s + 3)
                                                        command=self.rotation_click,
                                                        state="disabled")

                        # appends the created button to the horizontal rotation buttons list:
                        horizontal_rotation_buttons.append(rotation_button)

                # create the dots
                dot_button = create_button(image=dots_imgs[0],
                                           row=line + 1,
                                           column=column + 1,
                                           command=self.dot_click)
                self.dots[line].append({"id": dot_button,
                                        "background": 0,
                                        "location": (line, column)})

        direction_to_rotation_buttons_list = {
            self.directions[0]: horizontal_rotation_buttons,  # right
            self.directions[1]: vertical_rotation_buttons  # left
        }
        for quarter in range(4):
            for direction in self.directions:
                self.rotation_buttons[quarter][direction] = \
                    direction_to_rotation_buttons_list[direction][quarter]

        root.mainloop()

    def rotation_click(self, h):
        pass

    def dot_click(self, h):
        pass


def main():
    g = GraphicBoard()
    g.open_window()


if __name__ == '__main__':
    main()
