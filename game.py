import sys


# some helping functions:
def check_int(variable, question):
    """
    check if some variable is an int.
    if not, it asks the input <question> at raw_input
    :param variable: the checked variable
    :param question: if the variable isn't an int,
                     this will be the question at the 'raw_input'
    :return: the variable at <int> format, after checked.
    """

    while not str(variable).isdigit():  # check if the input was an int
        variable = raw_input(question)

    return int(variable)


class Board(object):

    def __init__(self, board_size=3, board_chr=0, board=None):

        self.board_size = board_size
        self.board_chr = board_chr
        if board:
            self.board = board
        else:
            self.board = self.create_empty_board()

    def rotate(self, direction):
        """
        rotates the board
        :param direction: 0: right rotation
                          1: left rotation
        :return: the rotated board according to the selected direction.
        """

        rotated_board = self.create_empty_board()  # the requested rotated board
        # the rotation algorithm:

        line_index = 0
        for line in range(
                (self.board_size - 1) * direction,
                (-1 * (self.board_size + 1)) * direction + self.board_size,
                -2 * direction + 1):

            column_index = 0
            for column in range(
                    direction * (1 - self.board_size) + self.board_size - 1,
                    direction * (self.board_size + 1) - 1,
                    2 * direction - 1):
                rotated_board[line_index][column_index] = self.board[column][line]

                column_index += 1

            line_index += 1

        return rotated_board

    def create_empty_board(self):
        """
        :return: an empty board
        """
        """
        board = []

        index = 1
        for line in range(self.board_size):
            board.append([])
            for column in range(self.board_size):
                board[line].append(index)
                index += 1"""
        board = [[self.board_chr for column in range(self.board_size)] for line in range(self.board_size)]

        return board


class Game(object):

    def __init__(self):
        self.local_boards = [Board(board_chr=(i + 1)) for i in range(4)]
        self.local_boards_size = self.local_boards[0].board_size
        self.main_board_size = (2 * self.local_boards_size)
        self.victory = None  # if someone wins, self.victory will be his player num
        # if there is a tie, self.victory will be 'tie'
        self.turn = 1
        """
        1 - player 1 turn
        2 - player 2 turn
        """
        self.players_chr = ["X", "O"]
        self.round = 1

        # note: there is no actual main board, it's theoretical

    def start_game(self):
        """
        starts the game
        :return: the winning player
        """
        while not self.victory:
            self.print_main_board()  # prints the board
            player_location = self.get_player_location()  # gets player's chr location
            self.place_on_board(player_location)  # places the entered location on one of the local boards
            self.print_main_board()  # prints the board
            self.rotate_quarter()  # rotates one quarter
            self.check_winner(self.turn)  # check if player 1 had won
            self.turn = 3 - self.turn  # flips player turn
            self.check_winner(self.turn)  # check if player 2 had won
            self.round += 1  # continues to the next round

        if self.victory == 'tie':
            print("tie...")
        else:
            print("Player {0} is the winner!!!".format(self.victory))

    def rotate_quarter(self):
        """
        A nice interface which asks you what quarter to rotate.
        """

        quarter_to_rotate = 10  # just a value to get into the while
        while not (0 < quarter_to_rotate < 5):
            quarter_to_rotate = "hi"  # makes sure that the while will not be an infinite loop
            quarter_to_rotate = check_int(quarter_to_rotate, "Enter quarter to rotate: ")
        quarter_to_rotate -= 1

        direction = raw_input("Enter the rotation direction (L, R): ").upper()
        while direction != "L" and direction != "R":
            direction = raw_input("Enter the rotation direction (L, R): ").upper()

        direction_format_convert = {  # converts the input ('L' or 'R') to num format.
            "R": 0,
            "L": 1
        }

        self.local_boards[quarter_to_rotate].board = self.local_boards[quarter_to_rotate].rotate(
            direction_format_convert[direction])  # actually rotating the quarter.

    def place_on_board(self, location):
        """
        place on one of the local board the player's chr
        :param location: type - local board location
               (quarter, line, column)
        """

        quarter = location[0]
        line = location[1]
        column = location[2]

        self.local_boards[quarter].board[line][column] = self.players_chr[self.turn - 1]  # places on board

    def get_player_location(self):
        """
        gets the player's chr location
        :return: the player location, type: <local board location>
                                            (quarter, line, column)
        """
        busy_place = True

        while busy_place:
            line = 10  # just a value to get into the while
            while not -1 < line < 6:
                line = "hi"  # makes sure that the while will not be an infinite loop
                line = check_int(line, "Enter line: ") - 1

            column = 10  # just a value to get into the while
            while not -1 < column < 6:
                column = "hi"  # makes sure that the while will not be an infinite loop
                column = check_int(column, "Enter column: ") - 1

            main_board_location = (line, column)

            local_board_location = self.main_location_to_local_location(main_board_location)
            if self.local_boards[local_board_location[0]].board[
                local_board_location[1]][local_board_location[2]] == self.local_boards[
                local_board_location[0]].board_chr:
                busy_place = False

        return local_board_location

    def main_location_to_local_location(self, location):
        """
        gets main board type location (line, column),
        and converts it to local board type location (quarter, line, column).
        :param location: main board type location
        :return: local board type location
        """

        main_line = location[0]
        main_column = location[1]
        half_board_size = (self.main_board_size / 2)

        # quarters division
        """
        quarters:
        0 1
        2 3
        """

        quarters_order = [[0, 1], [2, 3]]
        if main_line < half_board_size:
            quarter_line = 0
        else:
            quarter_line = 1

        if main_column < half_board_size:
            quarter_column = 0
        else:
            quarter_column = 1

        quarter = quarters_order[quarter_line][quarter_column]

        # lines and columns division

        line_converter = {
            0: 0,
            1: 0,
            2: half_board_size,
            3: half_board_size
        }

        column_converter = {
            0: 0,
            1: half_board_size,
            2: 0,
            3: half_board_size
        }

        local_line = main_line - line_converter[quarter]
        local_column = main_column - column_converter[quarter]

        local_location = (quarter, local_line, local_column)

        return local_location

    def print_main_board(self):

        print("Player {0}'s turn!\nRound {1}\n".format(self.turn, self.round))

        # print column index
        sys.stdout.write("   ")
        for column in range(self.main_board_size):
            sys.stdout.write(" {0}".format(column + 1))

        print("\n")

        # print the theoretical main board according to the local boards values
        main_board = self.create_main_board()  # main board
        matrix_main_board = main_board.board

        line_index = 1
        for line in matrix_main_board:
            sys.stdout.write(" {0}. ".format(line_index))
            for column in line:
                sys.stdout.write("{0} ".format(column))

            print("\n")
            line_index += 1

    def check_winner(self, player):
        """
        well...
        :return: if there is a winner, the player num.
                 if there isn't, None.
                 if there is a tie, 'tie'
        """
        main_board = self.create_main_board()  # main board
        matrix_main_board = main_board.board
        rotated_main_board = main_board.rotate(0)  # creates a rotated board,
        # in order to check the columns and decreasing diagonals.
        winning_list = [self.players_chr[player - 1] for i in range(self.main_board_size - 1)]

        def line_winner_check(board):
            """
            checks if there is a winner at some line on board.
            :param board: the checked board
            :return: if there is winning line, the player num.
                     if there isn't, None
            """
            board_size = len(board[0])
            line_index = 0
            for line in board:
                if (line[:board_size - 1] == winning_list) or (line[1:] == winning_list):
                    return player
                line_index += 1

            return None

        def diagonal_winner_check(board):
            """
            checks if there is a winner at some diagonal on board.
            :param board: the checked board
            :return: if there is winning diagonal, the player num.
                     if there isn't, None
            """

            board_size = len(board[0])

            check_num_to_diagonal = {
                0: (0, board_size - 1, board_size - 2),
                1: (0, board_size, board_size - 1),
                2: (1, board_size, board_size),
            }
            #       (starting line, max line, appended starting column)

            for check_num in range(3):
                check_list = []
                for line in range(check_num_to_diagonal[check_num][0],
                                  check_num_to_diagonal[check_num][1]):
                    check_list.append(board[line]
                                      [check_num_to_diagonal[check_num][2] - line])

                if check_list == winning_list:
                    return player

            return None

        # line winner check
        if line_winner_check(matrix_main_board):
            self.victory = player
            return player

        # column winner check
        if line_winner_check(rotated_main_board):
            self.victory = player
            return player

        # increasing diagonals winner check
        if diagonal_winner_check(matrix_main_board):
            self.victory = player
            return player

        # decreasing diagonals winner check
        if diagonal_winner_check(rotated_main_board):
            self.victory = player
            return player

        # check if there is a tie
        if self.round == self.main_board_size * self.main_board_size + 1:
            self.victory = 'tie'
            return 'tie'

        return None

    def create_main_board(self):
        """
        creates theoretical main board from the local boards.
        :return: theoretical main board, <Board> format
        """

        # creates the main board at matrix format
        matrix_main_board = []
        for board_half in range(2):
            for line in range(self.main_board_size / 2):
                matrix_main_board.append([])
                for quarter in range(2 * board_half, 2 * board_half + 2):
                    for quarter_column in range(self.local_boards_size):
                        matrix_main_board[line + (board_half * 3)] \
                            .append(self.local_boards[quarter].board[line][quarter_column])

        # creates the main board at <Board> format
        main_board = Board(board_size=self.main_board_size,
                           board=matrix_main_board)

        return main_board


def main():
    game = Game()
    game.start_game()


if __name__ == '__main__':
    main()
